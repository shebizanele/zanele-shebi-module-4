import 'package:flutter/material.dart';
import 'package:mod4/widgets/info_card.dart';

const email = "zert@gmail.com";
const phone = "076 887 0988";
const country = "South Africa";
const location = "Umlazi, Durban";

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Profile Page"),
        ),
        body: SafeArea(
          minimum: const EdgeInsets.only(top: 50),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/images/user.png'),
              ),
              Text(
                "Zanele Shebi",
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Text(
                "Flutter Beginner",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  letterSpacing: 2.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20,
                width: 200,
                child: Divider(
                  color: Colors.indigo,
                ),
              ),
              InfoCard(text: phone, icon: Icons.phone, onPressed: () async {}),
              InfoCard(text: email, icon: Icons.email, onPressed: () async {}),
              InfoCard(
                  text: country,
                  icon: Icons.location_city,
                  onPressed: () async {}),
              InfoCard(
                  text: location,
                  icon: Icons.location_city,
                  onPressed: () async {}),
            ],
          ),
        ));
  }
}
