import 'package:flutter/material.dart';
import 'splash.dart';
import 'profile.dart';
import 'loginscreen.dart';
import 'dash.dart';
void main() {
  //SystemChrome.setSystemUIOverlayStyle(
  //SystemUIOverlayStyle(statusBarColor: Colors.transparent));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ps Cakes',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.indigo,
          accentColor: Colors.indigo,
          scaffoldBackgroundColor: Colors.white),
      home: const SplashScreen(),
    );
  }
}
